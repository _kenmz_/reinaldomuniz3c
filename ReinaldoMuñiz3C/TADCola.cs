﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReinaldoMuñiz3C
{
    class TADCola
    {
        public static int max_lenght = 5;
        public int[] cola = new int[max_lenght];
        public int primero = -1;
        public int ultimo = -1;

        public int getMax_lenght() {
            return max_lenght;
        }
        public Boolean Vacia() {
            return (primero == -1 & ultimo == -1);
        }
        public void Encolar(int dato) {
            if (Vacia())
            {
                primero++;
                ultimo++;
                cola[ultimo] = dato;
            }
            else {
                if (ultimo < max_lenght-1)
                {
                    ultimo++;
                    if (ultimo != primero)
                    {
                    cola[ultimo] = dato;
                    }
                }else{

                    ultimo = -1;
                }
            }
        }

        public int Desencolar() {
            int valor = cola[primero];
            primero++;
            if (primero == max_lenght)
            {
                primero = 0;
            }
            
            return valor;
        }

        public int Ultimo() {
            return cola[ultimo];
        }

    }
}
