﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReinaldoMuñiz3C
{
    class Empleado : Persona
    {
        private int Id;
        private DateTime FechaDeIngreso;
        private int NDespacho;

        public Empleado(DateTime FechaDeIngreso, int NDespacho, string Nombre, string Apellido, string CI, string EstadoCivil) : base(Nombre, Apellido, CI, EstadoCivil)
        {
            this.FechaDeIngreso = FechaDeIngreso;
            this.NDespacho = NDespacho;
        }
        public int getId()
        {
            return Id;
        }

        public void setId(int Id)
        {
            this.Id = Id;
        }
        public DateTime getFechaDeIngreso()
        {
            return FechaDeIngreso;
        }

        public void setFechaDeIngreso(DateTime FechaDeIngreso)
        {
            this.FechaDeIngreso = FechaDeIngreso;
        }
        public int getNDespacho()
        {
            return NDespacho;
        }

        public void setNDespacho(int NDespacho)
        {
            this.NDespacho = NDespacho;
        }

        public void ReasignarDespacho(int DespachoNuevo) {
            setNDespacho(DespachoNuevo);
        }
    }
}
