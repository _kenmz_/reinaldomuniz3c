﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReinaldoMuñiz3C
{
    class TADPila
    {
        //Cambio check 2.2
        public static int max_lenght = 5;
        public static int [] pila = new int[max_lenght];
        public static int cima = -1;

        public Boolean Vacia() {
            return (pila.Length-1)==cima;
        }
        public void Apilar(int dato) {
            if (cima < max_lenght)
            {
                cima++;
                pila[cima] = dato;
            }
                
        }

        public int Desapilar() {
            int valor = Top();
            cima--;
            return valor;
        }

        public int Top() {
            return pila[cima];
        }

    }
}
