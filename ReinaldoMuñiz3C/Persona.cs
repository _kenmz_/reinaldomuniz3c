﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReinaldoMuñiz3C
{
    class Persona
    {
        private int Id;
        private string Nombre;
        private string Apellido;
        private string CI;
        private string EstadoCivil;

        public Persona(string Nombre, string Apellido, string CI, string EstadoCivil)
        {
            this.Nombre = Nombre;
            this.Apellido = Apellido;
            this.CI = CI;
            this.EstadoCivil = EstadoCivil;
        }

        public int getId()
        {
            return Id;
        }

        public void setId(int Id)
        {
            this.Id = Id;
        }
        public string getNombre() {
            return Nombre;
        }

        public void setNombre(string Nombre) {
            this.Nombre = Nombre;
        }
        public string getApellido()
        {
            return Apellido;
        }

        public void setApellido(string Apellido)
        {
            this.Apellido = Apellido;
        }
        public string getCI()
        {
            return CI;
        }

        public void setCI(string CI)
        {
            this.CI = CI;
        }

        public string getEstadoCivil()
        {
            return EstadoCivil;
        }

        public void setEstadoCivil(string EstadoCivil)
        {
            this.EstadoCivil = EstadoCivil;
        }

        public void CambiarEstadoCivil(string EstadoActual) {
            setEstadoCivil(EstadoActual);
        }

    }
}
