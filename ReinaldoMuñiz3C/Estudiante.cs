﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReinaldoMuñiz3C
{
    class Estudiante : Persona
    {
        private int Id;
        private int Curso;

        public Estudiante(int Curso, string Nombre, string Apellido, string CI, string EstadoCivil) : base(Nombre, Apellido, CI, EstadoCivil) {
            this.Curso = Curso;
        }

        public int getId()
        {
            return Id;
        }

        public void setId(int Id)
        {
            this.Id = Id;
        }
        public int getCurso()
        {
            return Curso;
        }

        public void setCurso(int Curso)
        {
            this.Curso = Curso;
        }

        public void Matriculacion(int CursoNuevo) {
            setCurso(CursoNuevo);
        }
        public string Info()
        {
            string info = "";
            info += "Datos del Estudiante:\nNombre y Apellido: " + getNombre() + " " + getApellido() + "\n";
            info += "Nº cedula: " + getCI() + "\n";
            info += "Estado civil: " + getEstadoCivil() + "\n";
            info += "Curso: " + getCurso() +  "\n";
            return info;
        }
    }
}
