﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReinaldoMuñiz3C
{
    class Profesor : Empleado
    {
        private int Id;
        private string Departamento;

        public Profesor(string Departamento, DateTime FechaDeIngreso, int NDespacho, string Nombre, string Apellido, string CI, string EstadoCivil): base(FechaDeIngreso, NDespacho, Nombre, Apellido, CI, EstadoCivil) {
            this.Departamento = Departamento;
        }
        public int getId()
        {
            return Id;
        }

        public void setId(int Id)
        {
            this.Id = Id;
        }
        public string getDepartamento()
        {
            return Departamento;
        }

        public void setDepartamento(string Departamento)
        {
            this.Departamento = Departamento;
        }

        public void CambiarDepartamento(string NuevoDepartamento) {
            setDepartamento(NuevoDepartamento);
        }

        public string Info() {
            string info = "";
            info +="Datos del profesor:\nNombre y Apellido: " + getNombre() + " " + getApellido()+"\n";
            info += "Nº cedula: " + getCI() + "\n";
            info += "Estado civil: " + getEstadoCivil()+"\n";
            info += "Fecha de ingreso: " + getFechaDeIngreso()+"\n";
            info += "Departamento: " + getDepartamento();
            return info;
        }
    }
}
