﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReinaldoMuñiz3C
{
    class PersonalServicio : Empleado
    {
        private int Id;
        private string Seccion;

        public PersonalServicio(string Seccion, DateTime FechaDeIngreso, int NDespacho, string Nombre, string Apellido, string CI, string EstadoCivil) : base(FechaDeIngreso, NDespacho, Nombre, Apellido, CI, EstadoCivil)
        {
            this.Seccion = Seccion;
        }
        public string getSeccion()
        {
            return Seccion;
        }

        public void setSeccion(string Seccion)
        {
            this.Seccion = Seccion;
        }

        public void Traslado(string SeccionNueva) {
            setSeccion(SeccionNueva);
        }

        public string Info()
        {
            string info = "";
            info += "Datos del empleado:\nNombre y Apellido: " + getNombre() + " " + getApellido() + "\n";
            info += "Nº cedula: " + getCI() + "\n";
            info += "Estado civil: " + getEstadoCivil() + "\n";
            info += "Fecha de ingreso: " + getFechaDeIngreso() + "\n";
            info += "Sección: " + getSeccion();
            return info;
        }
    }
}
