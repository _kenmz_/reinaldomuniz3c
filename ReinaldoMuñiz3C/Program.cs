﻿using System;

namespace ReinaldoMuñiz3C
{
    class Program
    {
        static void Main(string[] args)
        {
            //TADPila pila = new TADPila();
            //pila.Apilar(1);
            //pila.Apilar(2);
            //pila.Apilar(3);
            //pila.Apilar(4);
            //Console.WriteLine(pila.Desapilar());
            //Console.WriteLine(pila.Desapilar());
            //pila.Apilar(1);
            //Console.WriteLine(pila.Desapilar());
            //TADCola cola = new TADCola();
            //cola.Encolar(1);
            //cola.Encolar(2);
            //cola.Encolar(3);
            //cola.Encolar(4);
            //Console.WriteLine(cola.Desencolar());
            Estudiante estudiante = new Estudiante(1, "Reinaldo", "Muñiz", "1313461095", "Soltero");
            Console.WriteLine(estudiante.Info());
            Console.WriteLine("\n-------------------------\n");
            Profesor profesor = new Profesor("Lenguaje", new DateTime(2012, 5, 13), 233, "Alberto", "Gonzale", "0951584675", "Casado");
            Console.WriteLine(profesor.Info());
            Console.WriteLine("\n-------------------------\n");
            PersonalServicio personalServicio = new PersonalServicio("Biblioteca", new DateTime(2018, 2, 28), 125, "Maria", "Gutierrez", "1313485705", "Soltera");
            Console.WriteLine(personalServicio.Info());

        }
    }
}
