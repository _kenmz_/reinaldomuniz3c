﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReinaldoMuniz3C
{
    class EmpleadoPorHoras:Empleado
    {
        static private float precioDeHora = 2.50f;
        private int horasTrabajadas;

        public EmpleadoPorHoras(int horasTrabajadas, string apellidos, string nombres, int Edad, string Departamento) : base(apellidos, nombres, Edad, Departamento)
        {
            this.horasTrabajadas = horasTrabajadas;
        }

        public int getHorasTrabajadas() {
            return horasTrabajadas;
        }

        public void setHorasTrabajadas(int horasTrabajadas) {
            this.horasTrabajadas = horasTrabajadas;
        }


        //METODO
        public float CalcularSueldo() {
            float sueldo = horasTrabajadas * precioDeHora;
            return sueldo;
        }

    }
}
