﻿using System;

namespace ReinaldoMuniz3C
{
    //Autor: Kenny Reinaldo Muñiz Zambrano
    class Program
    {
        static void Main(string[] args)
        {
            string Empleados;
            Console.WriteLine("------------------EMPLEADO FIJO---------------------");
            EmpleadoFijo empleadoFijo = new EmpleadoFijo(new DateTime(2015, 6, 12), "Muniz", "Kenny", 18, "Developer");
            Empleados = "Nombre: " + empleadoFijo.getNombres() 
                      + "   Apellidos: " + empleadoFijo.getApellidos() 
                      +"  Edad:"+empleadoFijo.getEdad()
                      +"\nFecha de Entrada :"+empleadoFijo.getFechaDeEntrada() +"|     Tiempo Trabajado:"+empleadoFijo.CalcularAnos() +" años"
                      +"|    Sueldo:" + empleadoFijo.CalcularSueldo()+ "$";
            Console.WriteLine(Empleados);

            Console.WriteLine("\n---------------EMPLEADO TEMPORAL---------------------");
            EmpleadoTemporal empleadoTemporal = new EmpleadoTemporal(new DateTime(2021, 5, 1), new DateTime(2021, 10, 31), "Zambrano", "Kenny", 30, "Developer");
            Empleados = "Nombre: " + empleadoTemporal.getNombres()
                       + "  Apellidos: " + empleadoTemporal.getApellidos()
                       + "  Edad:" + empleadoTemporal.getEdad()
                       + "\nTiempo Trabajado:" + empleadoTemporal.CalcularMeses()+" meses"
                       + "|   Sueldo generado en "+ empleadoTemporal.getMesesTrabajados() +" meses:" + empleadoTemporal.CalcularSueldo()+" $";
            Console.WriteLine(Empleados);


            Console.WriteLine("\n---------------EMPLEADO POR HORAS---------------------");
            EmpleadoPorHoras empleadoPorHoras = new EmpleadoPorHoras(8, "Muniz", "Kenny", 22, "Developer");
            Empleados = "Nombre: " + empleadoPorHoras.getNombres()
                       + "  Apellidos: " + empleadoPorHoras.getApellidos()
                       + "  Edad:" + empleadoPorHoras.getEdad()
                       + "\nTiempo Trabajado:" + empleadoPorHoras.getHorasTrabajadas() + " horas"
                       + "|   Paga:" + empleadoPorHoras.CalcularSueldo() + " $";
            Console.WriteLine(Empleados);

        }
    }
}
