﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReinaldoMuniz3C
{
    class EmpleadoFijo : Empleado
    {
        private DateTime FechaDeEntrada;
        static private float sueldoMensual = 500;
        static private int complemento = 25;


        public EmpleadoFijo(DateTime FechaDeEntrada, string apellidos, string nombres, int Edad, string Departamento) : base(apellidos, nombres, Edad, Departamento) {
            this.FechaDeEntrada = FechaDeEntrada;
        }

        //getters and setters 
        public DateTime getFechaDeEntrada() {
            return FechaDeEntrada;
        }
        public void setmesesTrabajados(DateTime FechaDeEntrada) {
            this.FechaDeEntrada = FechaDeEntrada;
        }

        //METODO---------------
        public int CalcularAnos() {
            DateTime fechaActual = DateTime.Now;
            TimeSpan diasTrabajados = fechaActual - FechaDeEntrada;
            int dias = diasTrabajados.Days;
            int meses = dias / 30;
            int años = meses / 12;
            return años;
        }
        public float CalcularSueldo() {
            int sueldoComp = int.Parse(""+sueldoMensual) + complemento;
            int sueldo = sueldoComp * CalcularAnos();
            return sueldo;
        }
    }
}
