﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReinaldoMuniz3C
{
    class EmpleadoTemporal : Empleado
    {

        private int Id;
        private DateTime FechaDeIngreso;
        private DateTime FechaDeSalida;
        static private int sueldo = 399;
        private int mesesTrabajados;

        public EmpleadoTemporal(DateTime FechaDeIngreso, DateTime FechaDeSalida, string apellidos, string nombres, int Edad, string Departamento) : base(apellidos, nombres, Edad, Departamento)
        {
            this.FechaDeIngreso = FechaDeIngreso;
            this.FechaDeSalida = FechaDeSalida;
        }

        public int setId()
        {
            return Id;
        }
        public void getId(int Id)
        {
            this.Id = Id;
        }

        public DateTime getFechaDeIngreso()
        {
            return FechaDeIngreso;
        }

        public void setFechaDeIngreso(DateTime FechaDeIngreso)
        {
            this.FechaDeIngreso = FechaDeIngreso;
        }
        public DateTime getFechaDeSalida()
        {
            return FechaDeSalida;
        }

        public void setFechaDeSalida(DateTime FechaDeSalida)
        {
            this.FechaDeSalida = FechaDeSalida;
        }

        public int getMesesTrabajados() {
            return mesesTrabajados;
        }
        public void setMesesTrabajados(int mesesTrabajados) {
            this.mesesTrabajados = mesesTrabajados;
        }



        //METODOS
        public int CalcularMeses() {
            TimeSpan diferenciaDeFechas = FechaDeSalida - FechaDeIngreso;
            int dias = diferenciaDeFechas.Days;
            int meses = dias / 30;
            this.mesesTrabajados = meses;
            return meses;
        }

        public float CalcularSueldo()
        {
            int meses = CalcularMeses();
            float totalSueldo = meses * sueldo;
            return totalSueldo;
        }

    }
}
