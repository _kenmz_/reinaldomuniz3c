﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KennyMuniz3C
{
    public abstract class Persona
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string CI { get; set; }
        public string EstadoCivil { get; set; }


        public virtual void MostrarDatos() {
            Console.WriteLine("Nombre y Apellido: {0} {1}\tCedula: {2}\tEstado Civil: {3}", Nombre, Apellido, CI, EstadoCivil);
        }

        public abstract void CambiarEstadoCivil(string EstadoActual);
    }
}
