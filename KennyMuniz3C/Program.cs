﻿using System;
using System.Collections.Generic;
namespace KennyMuniz3C
{
    class Program
    {
        static void Main(string[] args)
        {
            ProfesorContrato Persona1 = new ProfesorContrato();
            Persona1.Nombre = "Alojo";
            Persona1.Apellido = "Mora";
            Persona1.CI = "1313131313";
            Persona1.EstadoCivil = "Casado";
            Persona1.Departamento = "Lenguaje";
            Persona1.FechaDeIngreso = new DateTime(2020, 2, 1);
            Persona1.HorasTrabajo = 24;
            Persona1.NDespacho = 13;

            ProfesorNombramiento Persona2 = new ProfesorNombramiento();
            Persona2.Nombre = "Alan";
            Persona2.Apellido = "Brito";
            Persona2.CI = "2323232323";
            Persona2.EstadoCivil = "Soltero";
            Persona2.Departamento = "Matematicas";
            Persona2.FechaDeIngreso = new DateTime(2018, 09, 1);
            Persona2.HorasExtras = 20;
            Persona2.NDespacho = 7;

            PersonalServicio Persona3 = new PersonalServicio();
            Persona3.Nombre = "Alan";
            Persona3.Apellido = "Brito";
            Persona3.CI = "3333333333";
            Persona3.EstadoCivil = "Soltero";
            Persona3.FechaDeIngreso = new DateTime(2018, 09, 1);
            Persona3.NDespacho = 7;
            Persona3.Seccion = "Biblioteca";

            List<Persona> listaPersonas = new List<Persona>();
            listaPersonas.Add(Persona1);
            listaPersonas.Add(Persona2);
            listaPersonas.Add(Persona3);

            foreach (Persona persona in listaPersonas)
            {
                persona.MostrarDatos();
                Console.WriteLine("------------------------------------------------------------");
            }

            Console.WriteLine("-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_");
            

        }
    }
}
