﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KennyMuniz3C
{
    public class Estudiante : Persona
    {
        public int Curso { get; set; }

        

        public void Matriculacion(int CursoNuevo) {
           Curso=CursoNuevo;
        }

        public override void MostrarDatos()
        {
            base.MostrarDatos();
            Console.WriteLine("Estudiante\nCurso Matriculado: {0}", Curso);
        }

        public override void CambiarEstadoCivil(string EstadoActual)
        {
            this.EstadoCivil = EstadoActual;
        }
    }
}
