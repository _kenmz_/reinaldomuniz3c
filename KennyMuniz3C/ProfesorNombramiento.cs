﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KennyMuniz3C
{
    public class ProfesorNombramiento : Profesor
    {
        public double ValorSueldo = 599;
        public double HorasExtras { get; set; }
        public override void CalcularSueldo()
        {
            double Sueldo = ValorSueldo+HorasExtras;
            Console.WriteLine("Sueldo " + Sueldo);
        }
        public override void CambiarDepartamento(string Departamento)
        {
            this.Departamento = Departamento;
        }

        public override void MostrarDatos()
        {
            base.MostrarDatos();
            Console.WriteLine("Horas Extras: {0}", HorasExtras);
        }
    }
}
