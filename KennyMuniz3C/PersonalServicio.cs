﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KennyMuniz3C
{
    public class PersonalServicio : Empleado
    {
        public string Seccion { get; set; }


        public override void ReasignarDespacho(int NDespacho)
        {
            this.NDespacho = NDespacho;

        }

        public void CambiarSeccion(string Seccion) {
            this.Seccion = Seccion;
        }

        public override void MostrarDatos()
        {
            base.MostrarDatos();
            Console.WriteLine("Personal de Servicio\nSeccion: {0}", Seccion);
        }

    }
}
