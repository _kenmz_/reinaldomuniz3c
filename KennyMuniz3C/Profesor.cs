﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KennyMuniz3C
{
    public abstract class Profesor : Empleado
    {
        public string Departamento { get; set; }

        public override void ReasignarDespacho(int NDespacho)
        {
            this.NDespacho = NDespacho;

        }

        public abstract void CambiarDepartamento(string Departamento);
        public abstract void CalcularSueldo();

        public override void MostrarDatos()
        {
            base.MostrarDatos();
            Console.WriteLine("Profesor\nDepartamento: {0}", Departamento);
        }


    }
}
