﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KennyMuniz3C
{
    public class ProfesorContrato : Profesor
    {
        public int HorasTrabajo { get; set; }
        public double ValorHora = 2.25;

        public override void CalcularSueldo()
        {
            double Sueldo = ValorHora * HorasTrabajo;
            Console.WriteLine("Sueldo "+Sueldo);
        }

        public override void CambiarDepartamento(string Departamento)
        {
            this.Departamento = Departamento;
        }
        public override void MostrarDatos()
        {
            base.MostrarDatos();
            Console.WriteLine("Horas trabajadas: {0}", HorasTrabajo);
        }


    }
}
