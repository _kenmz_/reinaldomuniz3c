﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KennyMuniz3C
{
    public abstract class Empleado : Persona
    {

        public DateTime FechaDeIngreso { get; set; }
        public int NDespacho { get; set; }


        public override void MostrarDatos()
        {
            base.MostrarDatos();
            Console.WriteLine("Fecha De Ingreso: {0}\tNumero de despacho: {1}", FechaDeIngreso, NDespacho);
        }


        public abstract void ReasignarDespacho(int NDespacho);

        public override void CambiarEstadoCivil(string EstadoActual)
        {
            this.EstadoCivil = EstadoActual;
        }

    }
}
